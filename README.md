# OpenML dataset: Earthquakes-Data-NZ

https://www.openml.org/d/43358

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
New Zealand lies on a fault-line that runs through its spine. This fault line aka Alpine Fault is very active and forms a part of the "Ring of Fire". 

Content
This is a list of all the earth quakes that occured in New Zealand between 01 Jan 2019 - 31 May 2020.

Acknowledgements
Thanks to GeoNet for making the data freely available to users. 

Inspiration
I live in NZ and quite often than not I experience these earthquakes. The earthquake data is made available to general public for free and because I love data  a beginner in data analysis / machine learning, I wondered if I can learn something from this data.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43358) of an [OpenML dataset](https://www.openml.org/d/43358). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43358/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43358/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43358/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

